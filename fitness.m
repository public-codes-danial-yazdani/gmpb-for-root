%******Generalized Moving Peaks Benchmark (GMPB) for Robust Optimization Over Time (ROOT)******
%Author: Danial Yazdani
% Email : danial.yazdani AT gmail.com
%Last Edited: December 06, 2021
%
% ------------
% Reference:
% ------------
%
%  D. Yazdani et al.,
%            "Benchmarking Continuous Dynamic Optimization: Survey and Generalized Test Suite,"
%            IEEE Transactions on Cybernetics (2020).
% 
% D. Yazdani et al.,
%            "Generalized Moving Peaks Benchmark," arXiv:2106.06174, (2021).
% 
% ------------
% Description:
% ------------
% 
%  This is the source code of the Generalized Moving Peaks Benchmark (GMPB),
%  which is proposed in the aforementioned refrences. This code includes
%  the non-modular version of GMPB for generating problem instances for 
%  Robust Optimization Over Time (ROOT). In this version of GMPB, each 
%  promising region has its own shift severity, height severity, and width
%  severity to generate different levels of robustness for each promising region.
%  Since some ROOT algorithms took advantage of the fixed shift length used
%  in the original MPB/GMPB, in this version, the center positions of the 
%  promising regions change by random dynamic (the shift step is generated 
%  by normal distribution) for fair comparisons.
% 
%  A simple single-swarm PSO is used as the optimizer. In this method, to 
%  address the diversity loss issie, a predefined percentage of the best 
%  particles are kept after each environmental changes, while the rest are
%  randomized across the search space. In this code, it is assumed that the
%  algorithm is informed about the environmental changes and it does not
%  need to detect them. When the deployed solution is no longer acceptable,
%  the Gbest position of PSO is deployed.
%
% -------
% Inputs:
% -------
% 
%    Set ROOT Threshold, dimension, number of promising regions,
%    maximum and minimum values of shift severity, change frequency, and budget
%    in "BenchmarkGenerator.m" for generating different problem instances with 
%    differnt characteristics and challenges.
% 
%    RunNumber      : Number of runs.
%
%
% --------
% Outputs:
% --------
%    Average survival time
%
% --------
% License:
% --------
% This program is to be used under the terms of the GNU General Public License 
% (http://www.gnu.org/copyleft/gpl.html).
% Author: Danial Yazdani
% e-mail: danial.yazdani AT gmail.com
% Copyright notice: (c) 2021 Danial Yazdani
%***********************************************************************************************
function [Results,GMPB,PSO,ROOT] = fitness(Solutions , GMPB , PSO , ROOT)
[SolutionNum,~]=size(Solutions);
Results=zeros(SolutionNum,1);
for i=1:SolutionNum
    if (GMPB.FE==GMPB.MaxEvals) || GMPB.RecentChange==1
        return;
    end
    x=Solutions(i,:)';
    f=NaN(1,GMPB.PeakNumber);
    for k=1 : GMPB.PeakNumber
        a = Transform((x - GMPB.PeaksPosition(k,:,GMPB.Environmentcounter)')'*GMPB.RotationMatrix{GMPB.Environmentcounter}(:,:,k)',GMPB.tau(GMPB.Environmentcounter,k),GMPB.eta(k,:,GMPB.Environmentcounter));
        b = Transform(GMPB.RotationMatrix{GMPB.Environmentcounter}(:,:,k) * (x - GMPB.PeaksPosition(k,:,GMPB.Environmentcounter)'),GMPB.tau(GMPB.Environmentcounter,k),GMPB.eta(k,:,GMPB.Environmentcounter));
        f(k) = GMPB.PeaksHeight(GMPB.Environmentcounter,k) - sqrt( a * diag(GMPB.PeaksWidth(k,:,GMPB.Environmentcounter).^2) * b);
    end
    Results(i,1) = max(f);
    GMPB.FE = GMPB.FE+1;
    if ~rem(GMPB.FE , GMPB.ChangeFrequency)
        GMPB.Environmentcounter = GMPB.Environmentcounter+1;
        GMPB.RecentChange = 1;
        return;
    end
    if (rem(GMPB.FE,GMPB.ChangeFrequency)==GMPB.Budget)
        %% Choosing the first solution for deployment in the first environment
        if GMPB.FE<GMPB.ChangeFrequency%RS for the 1st environment
            ROOT.Position                   = PSO.particles.GbestPosition;
            ROOT.FitnessValue               = PSO.particles.GbestValue;
            ROOT.SurvivalTimeSequence(1)    = 0;
        else
            %% Choosing the best found solution for deployment
            if ROOT.FitnessValue < GMPB.ROOTThreshold
                ROOT.Position = PSO.particles.GbestPosition;
                ROOT.FitnessValue = PSO.particles.GbestValue;
                ROOT.SurvivalTimeSequence(1,GMPB.Environmentcounter) = 0;
            else
                ROOT.SurvivalTimeSequence(1,GMPB.Environmentcounter) = ROOT.SurvivalTimeSequence(1,GMPB.Environmentcounter-1) + 1;
            end
        end
    end
end
