%******Generalized Moving Peaks Benchmark (GMPB) for Robust Optimization Over Time (ROOT)******
%Author: Danial Yazdani
% Email : danial.yazdani AT gmail.com
%Last Edited: December 06, 2021
%
% ------------
% Reference:
% ------------
%
%  D. Yazdani et al.,
%            "Benchmarking Continuous Dynamic Optimization: Survey and Generalized Test Suite,"
%            IEEE Transactions on Cybernetics (2020).
% 
% D. Yazdani et al.,
%            "Generalized Moving Peaks Benchmark," arXiv:2106.06174, (2021).
% 
% ------------
% Description:
% ------------
% 
%  This is the source code of the Generalized Moving Peaks Benchmark (GMPB),
%  which is proposed in the aforementioned refrences. This code includes
%  the non-modular version of GMPB for generating problem instances for 
%  Robust Optimization Over Time (ROOT). In this version of GMPB, each 
%  promising region has its own shift severity, height severity, and width
%  severity to generate different levels of robustness for each promising region.
%  Since some ROOT algorithms took advantage of the fixed shift length used
%  in the original MPB/GMPB, in this version, the center positions of the 
%  promising regions change by random dynamic (the shift step is generated 
%  by normal distribution) for fair comparisons.
% 
%  A simple single-swarm PSO is used as the optimizer. In this method, to 
%  address the diversity loss issie, a predefined percentage of the best 
%  particles are kept after each environmental changes, while the rest are
%  randomized across the search space. In this code, it is assumed that the
%  algorithm is informed about the environmental changes and it does not
%  need to detect them. When the deployed solution is no longer acceptable,
%  the Gbest position of PSO is deployed.
%
% -------
% Inputs:
% -------
% 
%    Set ROOT Threshold, dimension, number of promising regions,
%    maximum and minimum values of shift severity, change frequency, and budget
%    in "BenchmarkGenerator.m" for generating different problem instances with 
%    differnt characteristics and challenges.
% 
%    RunNumber      : Number of runs.
%
%
% --------
% Outputs:
% --------
%    Average survival time
%
% --------
% License:
% --------
% This program is to be used under the terms of the GNU General Public License 
% (http://www.gnu.org/copyleft/gpl.html).
% Author: Danial Yazdani
% e-mail: danial.yazdani AT gmail.com
% Copyright notice: (c) 2021 Danial Yazdani
%***********************************************************************************************
function [GMPB,PSO,ROOT] = PSOprocedure(GMPB,PSO,ROOT)
PSO.particles.Velocity = PSO.x * (PSO.particles.Velocity + ...
    (PSO.c1 * rand(PSO.PopulationSize , GMPB.Dimension).*(PSO.particles.PbestPosition - PSO.particles.position)) + ...
    (PSO.c2*rand(PSO.PopulationSize , GMPB.Dimension).*(repmat(PSO.particles.GbestPosition,PSO.PopulationSize,1) - PSO.particles.position)));
PSO.particles.position = PSO.particles.position + PSO.particles.Velocity;
for jj=1 : PSO.PopulationSize
    for kk=1 : GMPB.Dimension
        if PSO.particles.position(jj,kk) > GMPB.MaxCoordinate
            PSO.particles.position(jj,kk) = GMPB.MaxCoordinate;
            PSO.particles.Velocity(jj,kk) = 0;
        elseif PSO.particles.position(jj,kk) < GMPB.MinCoordinate
            PSO.particles.position(jj,kk) = GMPB.MinCoordinate;
            PSO.particles.Velocity(jj,kk) = 0;
        end
    end
end
[Results, GMPB , PSO , ROOT] = fitness(PSO.particles.position, GMPB , PSO , ROOT);
if GMPB.RecentChange==1
    return;
end
PSO.particles.FitnessValue = Results;
for jj=1 : PSO.PopulationSize
    if PSO.particles.FitnessValue(jj) > PSO.particles.PbestValue(jj)
        PSO.particles.PbestValue(jj) = PSO.particles.FitnessValue(jj);
        PSO.particles.PbestPosition(jj,:) = PSO.particles.position(jj,:);
    end
end
[BestPbestValue,BestPbestID] = max(PSO.particles.PbestValue);
if BestPbestValue>PSO.particles.GbestValue
    PSO.particles.GbestValue = BestPbestValue;
    PSO.particles.GbestPosition = PSO.particles.PbestPosition(BestPbestID,:);
end