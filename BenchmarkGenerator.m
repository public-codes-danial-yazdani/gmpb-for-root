%******Generalized Moving Peaks Benchmark (GMPB) for Robust Optimization Over Time (ROOT)******
%Author: Danial Yazdani
% Email : danial.yazdani AT gmail.com
%Last Edited: December 06, 2021
%
% ------------
% Reference:
% ------------
%
%  D. Yazdani et al.,
%            "Benchmarking Continuous Dynamic Optimization: Survey and Generalized Test Suite,"
%            IEEE Transactions on Cybernetics (2020).
% 
% D. Yazdani et al.,
%            "Generalized Moving Peaks Benchmark," arXiv:2106.06174, (2021).
% 
% ------------
% Description:
% ------------
% 
%  This is the source code of the Generalized Moving Peaks Benchmark (GMPB),
%  which is proposed in the aforementioned refrences. This code includes
%  the non-modular version of GMPB for generating problem instances for 
%  Robust Optimization Over Time (ROOT). In this version of GMPB, each 
%  promising region has its own shift severity, height severity, and width
%  severity to generate different levels of robustness for each promising region.
%  Since some ROOT algorithms took advantage of the fixed shift length used
%  in the original MPB/GMPB, in this version, the center positions of the 
%  promising regions change by random dynamic (the shift step is generated 
%  by normal distribution) for fair comparisons.
% 
%  A simple single-swarm PSO is used as the optimizer. In this method, to 
%  address the diversity loss issie, a predefined percentage of the best 
%  particles are kept after each environmental changes, while the rest are
%  randomized across the search space. In this code, it is assumed that the
%  algorithm is informed about the environmental changes and it does not
%  need to detect them. When the deployed solution is no longer acceptable,
%  the Gbest position of PSO is deployed.
%
% -------
% Inputs:
% -------
% 
%    Set ROOT Threshold, dimension, number of promising regions,
%    maximum and minimum values of shift severity, change frequency, and budget
%    in "BenchmarkGenerator.m" for generating different problem instances with 
%    differnt characteristics and challenges.
% 
%    RunNumber      : Number of runs.
%
%
% --------
% Outputs:
% --------
%    Average survival time
%
% --------
% License:
% --------
% This program is to be used under the terms of the GNU General Public License 
% (http://www.gnu.org/copyleft/gpl.html).
% Author: Danial Yazdani
% e-mail: danial.yazdani AT gmail.com
% Copyright notice: (c) 2021 Danial Yazdani
%***********************************************************************************************
function [GMPB] = BenchmarkGenerator
GMPB                     = [];
GMPB.FE                  = 0;
GMPB.PeakNumber          = 25;
GMPB.ChangeFrequency     = 2500;
GMPB.Dimension           = 5;
GMPB.ROOTThreshold       = 40;
GMPB.MinShiftSeverity    = 1;
GMPB.MaxShiftSeverity    = 5;
GMPB.ShiftSeverity       = (GMPB.MinShiftSeverity + (GMPB.MaxShiftSeverity-GMPB.MinShiftSeverity)*rand(GMPB.PeakNumber,1))/sqrt(GMPB.Dimension);
GMPB.Budget              = floor(GMPB.ChangeFrequency/2);
GMPB.EnvironmentNumber   = 100;
GMPB.Environmentcounter  = 1;
GMPB.RecentChange        = 0;
GMPB.MaxEvals            = GMPB.ChangeFrequency * GMPB.EnvironmentNumber;
GMPB.MinCoordinate       = -50;
GMPB.MaxCoordinate       = 50;
GMPB.MinHeight           = 30;
GMPB.MaxHeight           = 70;
GMPB.MinWidth            = 1;
GMPB.MaxWidth            = 12;
GMPB.MinAngle            = -pi;
GMPB.MaxAngle            = pi;
GMPB.MaxTau              = 0.4;
GMPB.MinTau              = 0;
GMPB.MaxEta              = 25;
GMPB.MinEta              = 10;
GMPB.MinHeightSeverity   = 1;
GMPB.MaxHeightSeverity   = 15;
GMPB.HeightSeverity      = GMPB.MinHeightSeverity + (GMPB.MaxHeightSeverity-GMPB.MinHeightSeverity)*rand(1,GMPB.PeakNumber);
GMPB.WidthSeverity       = ones(1,GMPB.PeakNumber);
GMPB.OptimumValue        = NaN(GMPB.EnvironmentNumber,1);
GMPB.PeaksHeight         = NaN(GMPB.EnvironmentNumber,GMPB.PeakNumber);
GMPB.PeaksPosition       = NaN(GMPB.PeakNumber,GMPB.Dimension,GMPB.EnvironmentNumber);
GMPB.PeaksWidth          = NaN(GMPB.PeakNumber,GMPB.Dimension,GMPB.EnvironmentNumber);
GMPB.PeaksPosition(:,:,1)= GMPB.MinCoordinate + (GMPB.MaxCoordinate-GMPB.MinCoordinate)*rand(GMPB.PeakNumber,GMPB.Dimension);
GMPB.PeaksHeight(1,:)    = GMPB.MinHeight + (GMPB.MaxHeight-GMPB.MinHeight)*rand(GMPB.PeakNumber,1);
GMPB.PeaksWidth(:,:,1)   = GMPB.MinWidth + (GMPB.MaxWidth-GMPB.MinWidth)*rand(GMPB.PeakNumber,GMPB.Dimension);
GMPB.OptimumValue(1)     = max(GMPB.PeaksHeight(1,:));
GMPB.AngleSeverity       = pi/9;
GMPB.TauSeverity         = 0.05;
GMPB.EtaSeverity         = 2;
GMPB.EllipticalPeaks     = 1;
GMPB.InitialRotationMatrix = NaN(GMPB.Dimension,GMPB.Dimension,GMPB.PeakNumber);
for ii=1 : GMPB.PeakNumber
    [GMPB.InitialRotationMatrix(:,:,ii) , ~] = qr(rand(GMPB.Dimension));
end
GMPB.MinWidthSeverity    = 0.1;
GMPB.MaxWidthSeverity    = 1.5;
GMPB.WidthSeverity       = GMPB.MinWidthSeverity + (GMPB.MaxWidthSeverity-GMPB.MinWidthSeverity)*rand(1,GMPB.PeakNumber);
GMPB.PeaksAngle          = GMPB.MinAngle + (GMPB.MaxAngle-GMPB.MinAngle)*rand(GMPB.PeakNumber,1);
GMPB.tau                 = GMPB.MinTau + (GMPB.MaxTau-GMPB.MinTau)*rand(GMPB.PeakNumber,1);
GMPB.RotationMatrix      = cell(1,GMPB.EnvironmentNumber);%NaN(GMPB.PeakNumber,GMPB.Dimension,GMPB.EnvironmentNumber);
GMPB.RotationMatrix{1}   = GMPB.InitialRotationMatrix;
GMPB.PeaksAngle          = NaN(GMPB.EnvironmentNumber,GMPB.PeakNumber);
GMPB.tau                 = NaN(GMPB.EnvironmentNumber,GMPB.PeakNumber);
GMPB.eta                 = NaN(GMPB.PeakNumber,4,GMPB.EnvironmentNumber);
GMPB.PeaksAngle(1,:)     = GMPB.MinAngle + (GMPB.MaxAngle-GMPB.MinAngle)*rand(GMPB.PeakNumber,1);
GMPB.tau(1,:)            = GMPB.MinTau + (GMPB.MaxTau-GMPB.MinTau)*rand(GMPB.PeakNumber,1);
GMPB.eta(:,:,1)          = GMPB.MinEta + (GMPB.MaxEta-GMPB.MinEta)*rand(GMPB.PeakNumber,4);
for ii=2 : GMPB.EnvironmentNumber
PeaksPosition           = NaN(GMPB.PeakNumber,GMPB.Dimension);
PeaksWidth              = NaN(GMPB.PeakNumber,GMPB.Dimension);
PeaksHeight             = NaN(1,GMPB.PeakNumber);
for jj=1 : GMPB.PeakNumber
    PeaksPosition(jj,:)  = GMPB.PeaksPosition(jj,:,ii-1) + (randn(1,GMPB.Dimension).* GMPB.ShiftSeverity(jj));
    PeaksWidth(jj,:)  = GMPB.PeaksWidth(jj,:,ii-1) + (randn(1,GMPB.Dimension).* GMPB.WidthSeverity(jj));
    PeaksHeight(1,jj)    = GMPB.PeaksHeight(ii-1,jj) + (GMPB.HeightSeverity(jj)*randn);
end
    PeaksAngle     = GMPB.PeaksAngle(ii-1,:) + (GMPB.AngleSeverity.*randn(1,GMPB.PeakNumber));
    PeaksTau     = GMPB.tau(ii-1,:) + (GMPB.TauSeverity.*randn(1,GMPB.PeakNumber));
    PeaksEta  = GMPB.eta(:,:,ii-1) + (randn(GMPB.PeakNumber,4).* GMPB.EtaSeverity);
    tmp = PeaksAngle > GMPB.MaxAngle;
    PeaksAngle(tmp) = (2*GMPB.MaxAngle)- PeaksAngle(tmp);
    tmp = PeaksAngle < GMPB.MinAngle;
    PeaksAngle(tmp) = (2*GMPB.MinAngle)- PeaksAngle(tmp);
    tmp = PeaksTau > GMPB.MaxTau;
    PeaksTau(tmp) = (2*GMPB.MaxTau)- PeaksTau(tmp);
    tmp = PeaksTau < GMPB.MinTau;
    PeaksTau(tmp) = (2*GMPB.MinTau)- PeaksTau(tmp);
    tmp = PeaksEta > GMPB.MaxEta;
    PeaksEta(tmp) = (2*GMPB.MaxEta)- PeaksEta(tmp);
    tmp = PeaksEta < GMPB.MinEta;
    PeaksEta(tmp) = (2*GMPB.MinEta)- PeaksEta(tmp);
    tmp = PeaksPosition > GMPB.MaxCoordinate;
    PeaksPosition(tmp) = (2*GMPB.MaxCoordinate)- PeaksPosition(tmp);
    tmp = PeaksPosition < GMPB.MinCoordinate;
    PeaksPosition(tmp) = (2*GMPB.MinCoordinate)- PeaksPosition(tmp);   
    tmp = PeaksHeight > GMPB.MaxHeight;
    PeaksHeight(tmp) = (2*GMPB.MaxHeight)- PeaksHeight(tmp);
    tmp = PeaksHeight < GMPB.MinHeight;
    PeaksHeight(tmp) = (2*GMPB.MinHeight)- PeaksHeight(tmp);  
    tmp = PeaksWidth > GMPB.MaxWidth;
    PeaksWidth(tmp) = (2*GMPB.MaxWidth)- PeaksWidth(tmp);
    tmp = PeaksWidth < GMPB.MinWidth;
    PeaksWidth(tmp) = (2*GMPB.MinWidth)- PeaksWidth(tmp);  
    GMPB.PeaksPosition(:,:,ii)= PeaksPosition;
    GMPB.PeaksHeight(ii,:)    = PeaksHeight;
    GMPB.PeaksWidth(:,:,ii)   = PeaksWidth;
    GMPB.PeaksAngle(ii,:)     = PeaksAngle;
    GMPB.tau(ii,:)            = PeaksTau;
    GMPB.eta(:,:,ii)          = PeaksEta;
    for jj=1 : GMPB.PeakNumber
        GMPB.RotationMatrix{ii}(:,:,jj) = GMPB.InitialRotationMatrix(:,:,jj) * Rotation(GMPB.PeaksAngle(ii,jj),GMPB.Dimension);
    end
    GMPB.OptimumValue(ii)     = max(PeaksHeight);
end
end